// vars
var date = document.querySelector("#id_date"),
    today = new Date(),
    dd = String(today.getDate()).padStart(2,'0'),
    mm = String(today.getMonth() + 1).padStart(2,'0'),
    yyyy = today.getFullYear(),
    // csrtoken 
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]'),
    // code field
    code_field = document.querySelector("#id_code"),
    // get inputs
    code_bill = document.querySelector("#id_code_fields"),
    decrip = document.querySelector("#id_decription"),
    price = document.querySelector("#id_price"),
    iva = document.querySelector("#id_iva"),
    amounts=document.querySelector("#id_amount"),
    descript = document.querySelector("#id_description"),
    container=document.querySelector("#container-btn"),
    container_data=document.querySelector("#container-datas"),
    url = '/facturacion/';
// auto execute
(() => {
    // format date
    current_day = `${dd}/${mm}/${yyyy}`;
    // pass date to view
    date.value = `${current_day}`;
    iva.value = ` ${15}%`;
})();
// send search
code_field.addEventListener("keyup", (e) => {
    if (e.keyCode === 'Enter' || e.keyCode === 13) {
        // ajax
        $.ajax({
            url:url,
            method:'POST',
            data: {
                code_arto:code_field.value,
                csrfmiddlewaretoken: csrtoken.value,
            },success:function(datas) {
                if (datas !== "") {
                    // pass data to fields
                    code_bill.value = datas.code;
                    decrip.value = datas.descrip;
                    price.value = `${datas.price}`;
                }
            }, error:function(err, req) {
                console.log(err, req);
            }
        });
    }
});

var press=false;
// Create bills and try to make number
amounts.addEventListener("keypress", () => {
    var btn = document.createElement("button");
    if (!press) {
        press = true;
        setTimeout( () => {
            btn.type="button";
            btn.textContent="Facturar";
            btn.className="btn btn-success";
            container.appendChild(btn);
        }, 1200);

        btn.onclick = function() {
            var subtotal = document.createElement("span"),
                tax = document.createElement("span"),
                total = document.createElement("span");
            setTimeout( () => {
                subtotal.innerHTML = `Sub total = ${price.value}`;
                results = amounts.value * price.value;
                tax.innerHTML = `IVA = ${results * 0.15}`;
                total.innerHTML = `Total = ${results + 0.15}`;
                subtotal.style.display="block";
                tax.style.display="block";
                container_data.appendChild(subtotal);
                container_data.appendChild(tax);
                container_data.appendChild(total);
            }, 1500);
        }
    }
});