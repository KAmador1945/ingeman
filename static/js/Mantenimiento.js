// globals
var sv_articles = document.getElementById('#CreateArticles'),
    code_articles = document.querySelector('#code_articles'),
    desc_articles = document.querySelector('#decription_articles'),
    price_articles = document.querySelector('#price_article'),
    cost_articles = document.querySelector('#cost_articles'),
    state_articles = document.querySelector('#state_arto'),
    csrtoken = document.querySelector('input[name="csrfmiddlewaretoken"]')
    url='/mantenimiento/create/',
    id;

function ShowModals() {
    //show modal
    $('#CreateArticles').modal('show');
}

// Create articles
function CreateArticles() {
    if (code_articles.value !== "" || desc_articles.value !== "" || price_articles.value !== "" || cost_articles.value !== "" || state_articles.value !== "") {
        // ajax 
        $.ajax({
            url: url,
            method:"POST",
            data: {
                codigo:code_articles.value,
                descripcion:desc_articles.value,
                precio:price_articles.value,
                costo:cost_articles.value,
                estado:state_articles.value,
                csrfmiddlewaretoken:csrtoken.value,
            }, success: function(message) {
                if (message.success !== "") {
                    alert(message.success)
                    setTimeout(() => {
                        window.location = '/mantenimiento/articles/';
                    }, 1500);
                }
            }, error: function(err, req) {
                console.log(err, req)
            }
        })
    }
}

function LoadInfoEdit(id_arto) {
    //show modal
    $('#EditArticles').modal('show');

    // define url 
    let url = '/mantenimiento/edit/',
        code = document.querySelector("#code_articles_edit"),
        descrip = document.querySelector("#decription_articles_edit"),
        price = document.querySelector("#price_article_edit"),
        cost = document.querySelector("#cost_articles_edit"),
        state = document.querySelector("#state_arto_edit");

    // get id
    id = id_arto;

    // ajax
    $.ajax({
        url:url,
        method:"GET",
        data: {
            id_articles:id_arto,
        }, success: function(result) {
            if (result !== "") {
                console.log(result);
                code.value = result.codigo;
                descrip.value = result.descripcion;
                price.value = result.precio;
                cost.value = result.costo;
                if (result.estado === true) {
                    state.checked = true;
                }
            } else {
                alert("Error");
            }
        }
    })
}

function ConfirmUpdateArticles() {
    let url = '/mantenimiento/edit/update/',
    code = document.querySelector("#code_articles_edit"),
    descrip = document.querySelector("#decription_articles_edit"),
    price = document.querySelector("#price_article_edit"),
    cost = document.querySelector("#cost_articles_edit"),
    csrtoken_update=document.querySelector('input[name="csrfmiddlewaretoken"]');
    state = document.querySelector("#state_arto_edit");
        // ajax
        $.ajax({
            url:url,
            method:"POST",
            data: {
                id_articles:id,
                code: code.value,
                descrip: descrip.value,
                price: price.value,
                cost: cost.value,
                state: state.value,
                csrfmiddlewaretoken: csrtoken_update.value,
            }, success: function(message) {
                if (message !== "") {
                   alert(message.success);
                   setTimeout(() => {
                       window.location='/mantenimiento/articles/';
                   }, 1200);
                } else {
                    alert("Error");
                }
            }
        })
}

function DeleteArticles(id_arto) {
    $('#DeleteArticles').modal('show');
    // get id
    id=id_arto;
}

function ConfirmDeleteArticle() {
    let url = '/mantenimiento/delete/',
    csrtoken_delete=document.querySelector('input[name="csrfmiddlewaretoken"]');
    $.ajax({
        url:url,
        method:'POST',
        data: {    
            id_articles:id,
            csrfmiddlewaretoken:csrtoken_delete.value,
        }, success: function(result) {
            alert(result.success);
            setTimeout(() => {
                window.location='/mantenimiento/articles/';
            }, 1200);
        }, error: function(err, req) {
            console.log(err, req);
        }
    })
}


