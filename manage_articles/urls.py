from django.urls import path
from manage_articles.views import ManageArticles, CreateArticles, LoadInfoEditArticles, DeleteArticles, UpdateInfoArticles

app_name="Mantenimiento"

urlpatterns=[
    path('articles/', ManageArticles, name="manatenimiento"),
    path('create/', CreateArticles, name="create"),
    path('edit/', LoadInfoEditArticles, name="edit"),
    path('edit/update/', UpdateInfoArticles, name="update"),
    path('delete/', DeleteArticles, name="delete"),
]