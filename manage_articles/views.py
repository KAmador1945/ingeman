from django.shortcuts import render
from manage_articles.forms import ManagesArticles
from manage_articles.models import Articulo
from django.http import JsonResponse

# Manage articles
def ManageArticles(request):
    # load form
    forms_man = ManagesArticles()
    # qs
    data = Articulo.objects.all()
    return render(request, 'mantenimiento/mantenimiento.html', {
        'mante':forms_man,
        'info':data,
    })

# create
def CreateArticles(request):
    # check method
    if request.method == 'POST':
        # capture data from js
        codigo_arto = request.POST['codigo']
        descripcion_arto = request.POST['descripcion']
        precio_arto = request.POST['precio']
        costo_arto = request.POST['costo']
        estado_arto = request.POST['estado']

        # check value of state
        if estado_arto == 'on':
            # save info
            data = Articulo(
                codigo=codigo_arto,
                precio=precio_arto,
                costo=costo_arto,
                descripcion=descripcion_arto,
            )

            # save method
            data.save()

            if data:
                message = {
                    'success':'Datos guardados'
                }
                
                return JsonResponse(message)
            else:
                message = {
                    'error':'Oops, error'
                }
                return JsonResponse(message)

# load objects in edit modal
def LoadInfoEditArticles(request):
    # check method
    if request.method == 'GET':
        id_arto = request.GET['id_articles']
        # qs
        data = Articulo.objects.all().get(id=id_arto)
        data.codigo
        # check if exist data
        if data != None:
            result = {
                'codigo':data.codigo,
                'precio':data.precio,
                'costo':data.costo,
                'estado':data.estado,
                'descripcion':data.descripcion,
            }

            return JsonResponse(result)
        else:
            result = {
                'error':'error, no se encontro nada'
            }

            return JsonResponse(result)

# update object
def UpdateInfoArticles(request):
    # check method
    if request.method == 'POST':
        # capture id
        id_arto = request.POST['id_articles']
        code = request.POST['code']
        descrip = request.POST['descrip']
        price = request.POST['price']
        cost = request.POST['cost']
        state = request.POST['state']

        print("actualiza ====>", type(id_arto))

        # detect state if is active or inactive
        if state == "":
            data = Articulo.objects.filter(id=id_arto).update(
                codigo=code,
                precio=price,
                costo=cost,
                descripcion=descrip,
                estado=False,
            )
            print(data)

            if data:
                message = {
                    'success':'Actualizado'
                }

                return JsonResponse(message)
            else:
                message = {
                    'error':'Actualizado'
                }

                return JsonResponse(message)
        else:
            data = Articulo.objects.filter(id=id_arto).update(
                codigo=code,
                precio=price,
                costo=cost,
                descripcion=descrip,
                estado=True,
            )
            print(data)

            if data:
                message = {
                    'success':'Actualizado'
                }

                return JsonResponse(message)
            else:
                message = {
                    'error':'Actualizado'
                }

                return JsonResponse(message)

# delete object
def DeleteArticles(request):
    if request.method == 'POST':
        id_arto = request.POST['id_articles']
        print("===>", id_arto)
        
        # delete object
        data = Articulo.objects.filter(id=id_arto).delete()

        # detect if qs was executed ok
        if data:
            message = {
                'success':'Elemento eliminado'
            }

            return JsonResponse(message)
        else:
            result = {
                'error':'error, no se encontro mada'
            }

            return JsonResponse(result)