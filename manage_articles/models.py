from django.db import models

# Create your models here.
class Articulo(models.Model):
    # define field
    codigo = models.CharField(max_length=20, blank=False)
    precio = models.CharField(max_length=50, blank=False)
    costo = models.CharField(max_length=50, blank=False)
    estado = models.BooleanField(default=True)
    descripcion = models.CharField(max_length=100, blank=False)
    created = models.DateTimeField(auto_now_add=True, blank=False)

    def __str__(self):
        return str(self.codigo)