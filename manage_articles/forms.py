from django import forms

class ManagesArticles(forms.Form):
    # create textfield
    code_article = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Codigo Articulo',
        })
    )
    descrip_article = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Descripcion'
        })
    )
    price_article = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Precio'
        })
    )
    cost_article = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'costo'
        })
    )
    state_article = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control'
        })
    )
