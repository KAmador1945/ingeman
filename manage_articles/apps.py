from django.apps import AppConfig


class ManageArticlesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'manage_articles'
