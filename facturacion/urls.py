from django.urls import path
from facturacion.views import Facturacion

app_name="Facturacion"
urlpatterns=[
    path('', Facturacion, name="facturacion"),
]