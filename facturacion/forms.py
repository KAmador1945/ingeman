from django import forms

# Create field code
class FieldCodeForm(forms.Form):
    # creating textfield
    code = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Type code',
        })
    )

class BiilsFields(forms.Form):
    code_fields = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'code'
        })
    )
    decription = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'description',
        })
    )
    price = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Price',
        })
    )
    amount = forms.CharField(
        widget=forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'amount',
            'type':'number',
        })
    )