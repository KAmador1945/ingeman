from django.shortcuts import render, redirect
from django.http import JsonResponse
from facturacion.forms import FieldCodeForm, BiilsFields
from manage_articles.models import Articulo

# Create your views here.
def Facturacion(request):   

    # load form
    if request.method == "GET":
        code_form = FieldCodeForm()
        bills = BiilsFields()

        return render(request, 'facturacion/facturacion.html',{
            'code':code_form,
            'bills':bills,
        })
    else:
        if request.method == "POST":
            code_article = request.POST['code_arto']
            find_data = Articulo.objects.all().get(codigo=code_article)
            if find_data != None:
                # load info in form
                datas = {
                    'code':find_data.codigo,
                    'descrip':find_data.descripcion,
                    'price':find_data.precio,
                }
                return JsonResponse(datas)
            else:
                message = {
                    'error':'No existe'
                }

                return JsonResponse(message)
        
